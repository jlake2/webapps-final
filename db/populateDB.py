#Use this script to create the database tables
#This script does NOT populate the tables with any data

import mysql.connector

#Define database variables
DATABASE_USER = 'root'
DATABASE_HOST = '127.0.0.1'
DATABASE_NAME = 'final_DB'

#Create connection to MySQL
cnx = mysql.connector.connect(user=DATABASE_USER, host=DATABASE_HOST)
cursor = cnx.cursor()

###################################
## Create DB if it doesn't exist ##
###################################

createDB = (("CREATE DATABASE IF NOT EXISTS %s DEFAULT CHARACTER SET latin1") % (
DATABASE_NAME))
cursor.execute(createDB)

#########################
## Switch to feednd DB ##
#########################

useDB = (("USE %s") % (DATABASE_NAME))
cursor.execute(useDB)

###################################
## Populate DB with sample input ##
###################################

deleteUser = ("DELETE from users where email='zlipp@nd.edu'")
cursor.execute(deleteUser)

addUser = ("INSERT INTO users(password, name, email) values('WebApps2015!','Zach Lipp', 'zlipp@nd.edu')")
cursor.execute(addUser)

getUserId = ("SELECT userID from users where email = 'zlipp@nd.edu'")
cursor.execute(getUserId)
userID = cursor.fetchone()[0]

addQuote = "INSERT INTO quotes(userID, text, author, date, score) values(%s, 'To be or not to be? That is the question', 'William Shakespeare', NOW(), 0) " % str(userID)
cursor.execute(addQuote)

cnx.commit()
cnx.close()
