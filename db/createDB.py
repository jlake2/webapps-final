#Use this script to create the database tables
#This script does NOT populate the tables with any data

import mysql.connector

#Define database variables
DATABASE_USER = 'root'
DATABASE_HOST = '127.0.0.1'
DATABASE_NAME = 'final_DB'

#Create connection to MySQL
cnx = mysql.connector.connect(user=DATABASE_USER, host=DATABASE_HOST)
cursor = cnx.cursor()

###################################
## Create DB if it doesn't exist ##
###################################

createDB = (("CREATE DATABASE IF NOT EXISTS %s DEFAULT CHARACTER SET latin1") % (DATABASE_NAME))
cursor.execute(createDB)

#########################
## Switch to feednd DB ##
#########################

useDB = (("USE %s") % (DATABASE_NAME))
cursor.execute(useDB)

###########################
## Drop all tables first ##
###########################

#quotes
dropTableQuery = ("DROP TABLE IF EXISTS quotes")
cursor.execute(dropTableQuery)


#Users
dropTableQuery = ("DROP TABLE IF EXISTS users")
cursor.execute(dropTableQuery)

########################
## Create tables next ##
########################

createTableQuery = ('''CREATE TABLE quotes (
						quoteID int NOT NULL AUTO_INCREMENT,
						text VARCHAR(200) NOT NULL,
						poster VARCHAR(45) NOT NULL,
						author VARCHAR(45) NOT NULL,
						date VARCHAR(20) NOT NULL,
						score int NOT NULL,
						PRIMARY KEY (quoteID)
					     );'''
                    )
cursor.execute(createTableQuery)

#Commit the data and close the connection to MySQL
cnx.commit()
cnx.close()
