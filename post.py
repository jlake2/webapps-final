''' Implements handler for /post
'''

import apiutil
import sys
import os.path
sys.stdout = sys.stderr # Turn off console output; it will get logged by Apache
import threading
import cherrypy
import os
import os.path
import json
import mysql.connector
from mysql.connector import Error
from jinja2 import Environment, FileSystemLoader
env = Environment(loader=FileSystemLoader(os.path.abspath(os.path.dirname(__file__))+'/templates/'))
import logging
from config import conf
from quotes import Quotes

class Post(object):
    ''' Handles resources /Quotes
        Allowed methods: GET, POST, PUT, DELETE '''
    exposed = True

    def __init__(self):
	self.db = {}
        self.db['name']='final_DB'
        self.db['user']='root'
        self.db['host']='127.0.0.1'

    def _cp_dispatch(self, vpath):
	cherrypy.request.params['quoteID'] = vpath.pop(0)
	cherrypy.request.params['vote'] = vpath.pop(0)
	return self

    def GET(self):
        # Get page to fill out form to submit vote
        # Get form to 
        # Fail with a status of 406 Not Acceptable if not HTML or JSON
        output_format = cherrypy.lib.cptools.accept(['text/html', 'application/json'])

        if output_format == 'text/html':
            return env.get_template('post.html').render(
                base=cherrypy.request.base.rstrip('/') + '/'
            )
        else:
            return json.dumps(result, encoding='utf-8')

    def POST(self, **kwargs):
	# Add a quote
	# keys in kwargs: poster-input, quote-input, author-input

	data = {}
	for key, value in kwargs.items():
	  data[key] = value

	try:
	  cnx = mysql.connector.connect(
		user=self.db['user'],
		host=self.db['host'],
                database=self.db['name'],
            )
	  cursor = cnx.cursor()
	  q="Insert into quotes(text, poster, author, date, score) values(%(quote-input)s, %(poster-input)s, %(author-input)s, NOW(), 0)"
	  
	  cursor.execute(q,data)      
	  cnx.commit()
	  cnx.close()

	  raise cherrypy.HTTPRedirect("/quotes")
	  	
	  return json.dumps(data)
	except Error as E:
	  return errorJSON(code=9001, message='Failed to add quote') 

    def OPTIONS(self,restID):
        return "<p>/post allows GET and POST</p>"

application = cherrypy.Application(Post(), None, conf)
