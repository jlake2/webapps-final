/* Wait until the DOM loads by wrapping our code in a callback to $. */
$(function() {

  /* Add click event listeners to the restaurant list items. This adds a
   * handler for each element matching the CSS selector
   * .restaurant-list-item. */
  $('.quotes-list-item a').click(function(event) {


    /* Prevent the default link navigation behavior. */
    event.preventDefault();

    var arrow = $(this);
    // vote via Ajax. 
    $.ajax({
      type: 'PUT',
      url: arrow.attr('href'),
      dataType: 'json'
    }).done(function(data) {
	//change number
    	var countElement = arrow.closest('.vote-div').find('.vote-count');	
    	var count = parseInt(countElement[0].innerHTML);

	//countElement.empty();
	var newCount = count + parseInt(data);
	countElement[0].innerHTML = newCount;
  
    }).fail(function() {
	//alert
        // Create an alert box. 
        var $alert = (
          $('<div>')
            .text('Whoops! Something went wrong.')
            .addClass('alert')
            .addClass('alert-danger')
            .addClass('alert-dismissible')
            .attr('role', 'alert')
            .prepend(
              $('<button>')
                .attr('type', 'button')
                .addClass('close')
                .attr('data-dismiss', 'alert')
                .html('&times;')
            )
            .hide()
        );
        // Add the alert to the alert container. 
        $('#alerts').append($alert);
        // Slide the alert into view with an animation. 
        $alert.slideDown();
	
    });
    
  });
});
