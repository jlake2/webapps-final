''' Implements handler for /vote
'''

import apiutil
import sys
import os.path
sys.stdout = sys.stderr # Turn off console output; it will get logged by Apache
import threading
import cherrypy
import os
import os.path
import json
import mysql.connector
from mysql.connector import Error
from jinja2 import Environment, FileSystemLoader
env = Environment(loader=FileSystemLoader(os.path.abspath(os.path.dirname(__file__))+'/templates/'))
import logging
from config import conf


class Vote(object):
    ''' Handles resources /Quotes
        Allowed methods: GET, POST, PUT, DELETE '''
    exposed = True

    def __init__(self):
	self.db = {}
        self.db['name']='final_DB'
        self.db['user']='root'
        self.db['host']='127.0.0.1'

    def _cp_dispatch(self, vpath):
	cherrypy.request.params['quoteID'] = vpath.pop(0)
	cherrypy.request.params['vote'] = vpath.pop(0)
	return self

    #return all quotes sorted by date
    def PUT(self, quoteID, vote):
	
        cnx = mysql.connector.connect(
            user=self.db['user'],
            host=self.db['host'],
            database=self.db['name'],
        )
        cursor = cnx.cursor()
	if( vote == "up"):
            qn="UPDATE quotes SET score = score + 1 WHERE quoteID= %s" % quoteID
	    ret = '1';
	else: 
	    qn="UPDATE quotes SET score = score - 1 WHERE quoteID= %s" % quoteID 
	    ret = '-1';
        cursor.execute(qn)
	cnx.commit()
	cnx.close()
	return json.dumps(ret)
	

    def OPTIONS(self):
        return "<p>/votes allows PUT and OPTIONS</p>"

application = cherrypy.Application(Vote(), None, conf)
