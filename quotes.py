''' Implements handler for /categories
Imported from handler for /restaurants/{id} '''

import apiutil
import sys
import os.path
sys.stdout = sys.stderr # Turn off console output; it will get logged by Apache
import threading
import cherrypy
import os
import os.path
import json
import mysql.connector
from mysql.connector import Error
from jinja2 import Environment, FileSystemLoader
env = Environment(loader=FileSystemLoader(os.path.abspath(os.path.dirname(__file__))+'/templates/'))
import logging
from config import conf


class Quotes(object):
    ''' Handles resources /Quotes
        Allowed methods: GET, POST, PUT, DELETE '''
    exposed = True

    def __init__(self):
	self.db = {}
        self.db['name']='final_DB'
        self.db['user']='root'
        self.db['host']='127.0.0.1'

    #return all quotes sorted by date
    def getDataFromDB(self):
        cnx = mysql.connector.connect(
            user=self.db['user'],
            host=self.db['host'],
            database=self.db['name'],
        )
        cursor = cnx.cursor()
        qn="select quoteID, text, author, date, score, poster from quotes order by score desc" 
        cursor.execute(qn)
        result=cursor.fetchall()
	cnx.close()
	
	ret = []
	for row in result:
		entry = {}
		entry['quoteID'] = row[0]
		entry['text'] = row[1]
		entry['author'] = row[2]
		entry['date'] = row[3]
		entry['score'] = row[4]
		entry['poster'] = row[5]
		ret.append(entry)
        return ret

    def GET(self):
        ''' Return list of quotes'''

        # Return data in the format requested in the Accept header
        # Fail with a status of 406 Not Acceptable if not HTML or JSON
        output_format = cherrypy.lib.cptools.accept(['text/html', 'application/json'])

        try:
            result=self.getDataFromDB()
        except mysql.connector.Error as e:
            logging.error(e)
            raise

        if output_format == 'text/html':
            return env.get_template('quotes.html').render(
                quotes=result,
                base=cherrypy.request.base.rstrip('/') + '/'
            )
        else:
            return json.dumps(result, encoding='utf-8')

    def OPTIONS(self,restID):
        return "<p>/quotes allows GET and OPTIONS</p>"

application = cherrypy.Application(Quotes(), None, conf)
