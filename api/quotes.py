#John F. Lake, Jr. 
#This will be the handler for /quotes/

import apiutil
import sys
import os.path
sys.stdout = sys.stderr # Turn off console output; it will get logged by Apache
import threading
import cherrypy
import os
import os.path
import math
import json
from collections import OrderedDict
import mysql.connector
from mysql.connector import Error
from jinja2 import Environment, FileSystemLoader
env = Environment(loader=FileSystemLoader(os.path.abspath(os.path.dirname(__file__))+'/templates/'))
import logging
from config import conf

class Quotes(object):
	''' Handles resource /restaurants
	Allowed methods: GET, POST, OPTIONS '''
	exposed = True

	def __init__(self):
		self.myd = dict()
		self.xtra = dict()
		self.db = dict()
		self.db['name']='final_DB'
		self.db['user']='root'
		self.db['host']='127.0.0.1'

	def _cp_dispatch(self,vpath):
		return vpath

	def getDataFromDB(self):
		return "Getting data from DB."

	def GET(self):
		''' Get list of quotes '''
		output_format = cherrypy.lib.cptools.accept(['text/html', 'application/json'])
		testquotes = {}
		testquotes['1'] = 'This is a quote!'
		testquotes['2'] = 'This is another quote!'
		testquotes['3'] = 'This is a third quote!'
		testquotes['4'] = 'This is a fourth quote!'
		testquotes['5'] = 'This is a fifth quote!'
		

		if output_format == 'text/html':
		    return env.get_template('quotes-tmpl.html').render(
			quotes=testquotes,
			base=cherrypy.request.base.rstrip('/') + '/'
		    )
		else:
		    return json.dumps(self.data, encoding='utf-8')

	def POST(self, **kwargs):
		return "You are attempting to POST."

	def OPTIONS(self):
		return "<p>/quotes/ allows GET, POST, and OPTIONS</p>"

class StaticAssets(object):
    pass

if __name__ == '__main__':
    conf = {
        'global': {
            'request.dispatch': cherrypy.dispatch.MethodDispatcher(),
            'tools.staticdir.root': os.path.dirname(os.path.abspath(__file__))
        }
    }
    cherrypy.tree.mount(Quotes(), '/quotes', {
        '/': {
            'request.dispatch': cherrypy.dispatch.MethodDispatcher()
        }
    })
    cherrypy.tree.mount(StaticAssets(), '/', {
        '/': {
            'tools.staticdir.root': os.path.dirname(os.path.abspath(__file__))
        }    
    })
    cherrypy.engine.start()
    cherrypy.engine.block()
else:
    application = cherrypy.Application(Quotes(), None, conf)

